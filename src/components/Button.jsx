import React from 'react'

export default function Button({children, className, label, onClick}) {
  return (
    <button className={`button ${className || ""}`} onClick={onClick}>
      {children}
      {label?<span className='button__label'>{label}</span>:''}
    </button>
  )
}
