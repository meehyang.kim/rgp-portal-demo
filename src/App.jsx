import { useEffect, useState, useRef } from "react";
import { useSearchParams } from "react-router-dom";
import Button from "./components/Button";
import ForcerModal from "./components/ForcerModal";
import TerritoryModal from "./components/TerritoryModal";
import useGameIframeURL from "./hooks/useGameIframeURL";
import {
  PLATFORM_CONFIG_URL,
  QUERY_PARAMS,
} from "./lib/constants";
import { IoLogoGameControllerA, IoIosClose } from "react-icons/io";
import { FaLanguage, FaDesktop, FaMobileAlt } from "react-icons/fa";
import "./App.css";

function App() {
  const [searchParams] = useSearchParams();
  const [gameConfig, setGameConfig] = useState({});
  const [gameKey, setGameKey] = useState(QUERY_PARAMS.gameKey);
  const [territory, setTerritory] = useState({
    country: QUERY_PARAMS.country,
    language: QUERY_PARAMS.language,
    currency: QUERY_PARAMS.currency,
  });
  const [isMobile, setIsMobile] = useState(QUERY_PARAMS.isMobile);
  const iframeURL = useGameIframeURL({
    gameKey: gameKey,
    isMobile: isMobile,
    ...territory,
  });
  const [forcerList, setForcerList] = useState([]);
  const [forcers, setForcers] = useState("");
  const [showForcerModal, setShowForcerModal] = useState(false);
  const [showTerritoryModal, setShowTerritoryModal] = useState(false);
  const gameIframe = useRef(null);

  /** functions */
  function toggleForcerModal() {
    setShowForcerModal((prev) => !prev);
  }

  function toggleTerritoryModal() {
    setShowTerritoryModal((prev) => !prev);
  }

  function createForcer(selectedForcer) {
    const { backendId } = gameConfig;
    const { memberId, website } = QUERY_PARAMS;
    const forcers = selectedForcer.value.split("\n");

    forcers.forEach(forcer => {
      // create forcer URL
      fetch(
        `${PLATFORM_CONFIG_URL}/comms-api/v1/${website}/${backendId}/forcer`,
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            gameKey,
            memberId,
            website,
            result: forcer,
          }),
        }
      )
        .then((res) => res.json())
        .then((res) => {
          const payload = {
            requestId: "",
            data: {
              resultKey: res["resultKey"],
              forceForEvents: [],
            },
          };
          const message = JSON.stringify({
            event: "addForcedResultKey",
            payload,
          });
  
          gameIframe.current.contentWindow.postMessage(message, "*");
  
          setForcers((prev) => (prev += `\n${res.resultKey}`));
        });
    });
  }

  /** hooks */
  useEffect(() => {
    // set gameKey from searchParams
    const { gameKey } = Object.fromEntries([...searchParams]);
    // @TODO get gamename

    if (!gameKey) return;
    setGameKey(gameKey);

    // mobile
    const mobileRegex =
      /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i;
    if (mobileRegex.test(navigator.userAgent)) {
      setIsMobile(true);
    }
  }, []);

  // 2. get game config by the gameKey & others...
  useEffect(() => {
    const fetchGameConfig = async () => {
      const response = await fetch(
        `${PLATFORM_CONFIG_URL}/game-configuration-api/v2/configurations/context/frontend/gamesysgames/${gameKey}/platforms/desktop?currency=${territory.currency}`
      );
      const gameConfig = await response.json();
      setGameConfig(gameConfig);
    };

    // @FIXME find a better way...
    if (!gameKey) return;
    fetchGameConfig();
  }, [gameKey]);

  // 3. get forcers
  useEffect(() => {
    const { forcerPrefix, forcerResources } = gameConfig;

    const fetchForcers = async () => {
      const response = await fetch(
        `${QUERY_PARAMS.wrapperHost}/${forcerPrefix}${
          forcerResources.default
        }/forcer.json?cachebust=${Date.now()}`
      );
      const forcers = await response.json();
      setForcerList(forcers);
    };

    // @FIXME find a better way...
    if (gameConfig && forcerPrefix && forcerResources) {
      fetchForcers();
    }
  }, [gameKey, gameConfig]);

  return (
    <div className="App">
      <header className="header">
        <nav className="header__nav">
          <Button
            className="button--wide"
            label="FORCER"
            onClick={toggleForcerModal}
          >
            <IoLogoGameControllerA />
          </Button>
          <Button
            className="button--wide"
            label="TERRITORY"
            onClick={toggleTerritoryModal}
          >
            <FaLanguage />
          </Button>
        </nav>
        <h1 className="header__title">{gameConfig.gameName || gameKey}</h1>
        <nav className="header__nav">
          <Button
            className={`button--desktop${!isMobile ? " active" : ""}`}
            onClick={() => {
              setIsMobile(false);
            }}
          >
            <FaDesktop />
          </Button>
          <Button
            className={`button--mobile${isMobile ? " active" : ""}`}
            onClick={() => {
              setIsMobile(true);
            }}
          >
            <FaMobileAlt />
          </Button>
          <Button className="button--close">
            <IoIosClose />
          </Button>
        </nav>
      </header>
      <ul
        style={{
          top: "30%",
          height: "auto",
          position: "absolute",
          zIndex: 123,
        }}
      >
        <li>
          <b>gameKey</b>: {gameKey}
        </li>
        <li>
          <b>country</b>: {territory.country}
        </li>
        <li>
          <b>currency</b>: {territory.currency}
        </li>
        <li>
          <b>lannguage</b>: {territory.language}
        </li>
        <li>
          <b>device</b>: {isMobile ? "MOBILE" : "DESKTOP"}
        </li>
        <li>
          <b>forcers</b>: <pre>{forcers}</pre>
        </li>
      </ul>
      <div className="gamescreen">
        <div className={`gamescreen__wrapper ${isMobile ? "mobile" : ""}`}>
          <iframe
            className="gameiframe"
            ref={gameIframe}
            src={iframeURL}
          ></iframe>
        </div>
      </div>
      <ForcerModal
        title={"FORCER"}
        items={forcerList}
        isShowing={showForcerModal}
        onClose={toggleForcerModal}
        onSubmit={createForcer}
      ></ForcerModal>
      <TerritoryModal
        isShowing={showTerritoryModal}
        onClose={toggleTerritoryModal}
        onSubmit={setTerritory}
      ></TerritoryModal>
    </div>
  );
}

export default App;
