import { useEffect, useState } from "react";
import { QUERY_PARAMS } from "../lib/constants";

const useGameIframeURL = ({ gameKey, currency, country, language, isMobile }) => {
  const [gameIframeURL, setGameIframeURL] = useState("");

  useEffect(() => {
    const queryParams = {
      ...QUERY_PARAMS,
      gameKey,
      currency,
      country,
      language,
      isMobile,
    };

    const queryString = Object.entries(queryParams)
      .map(([key, value]) => `${key}=${encodeURIComponent(value)}`)
      .join("&");

    const url = `${QUERY_PARAMS.wrapperHost}/static-assets/gs-wrapper/index.html?${queryString}`;
    setGameIframeURL(url);

  }, [gameKey, currency, country, language, isMobile]);

  return gameIframeURL;
};

export default useGameIframeURL;