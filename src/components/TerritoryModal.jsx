import React, { useEffect, useState } from "react";

export default function TerritoryModal({ isShowing, onSubmit, onClose }) {
  const [selectedCountry, setSelectedCountry] = useState("GB");
  const [selectedLanguage, setSelectedLanguage] = useState("en");
  const [selectedCurrency, setSelectedCurrency] = useState("GBP");
  const [countries, setCountries] = useState(["GB", "ES", "ONT"]);
  const [languages, setLanguages] = useState(["en"]);
  const [currencies, setCurrencies] = useState(["GBP"]);

  const handleOnclick = () => {
    onSubmit({
      country: selectedCountry,
      language: selectedLanguage,
      currency: selectedCurrency,
    });
    onClose();
  };

  const closeModal = () => {
    onClose();
  };

  useEffect(() => {
    setSelectedCountry(selectedCountry);
    switch (selectedCountry) {
      case "ES":
        setLanguages(["es", "es1", "es2"]);
        setCurrencies(["EUR", "EUR1", "EUR2"]);
        break;
      case "ONT":
        setLanguages(["en", "en-ONT"]);
        setCurrencies(["CND", "CND1", "CND2"]);
        break;
      case "GB":
        setSelectedCountry(selectedCountry);
        setLanguages(["en", "en-GB"]);
        setCurrencies(["GBP", "GBP1", "GBP2"]);
      // no break
      default:
        break;
    }
  }, [selectedCountry]);

  useEffect(() => {
    setSelectedLanguage(languages[0]);
  }, [languages]);

  useEffect(() => {
    setSelectedCurrency(currencies[0]);
  }, [currencies]);

  if (!isShowing) return;

  return (
    <div className="modal-container">
      <div className="modal-col">
        <div className="modal">
          <div className="modal-content">
            <header className="modal-header">
              <h1 className="modal-title">
                Choose the Territory you want to demo
              </h1>
              <button className="modal-close" onClick={closeModal}>
                X
              </button>
            </header>
            <div style={{ display: "flex", "flexDirection": "column", "marginTop": "20px" }}>
              <label htmlFor="country">Country</label>
              <select
                name="country"
                id="country"
                defaultValue={selectedCountry ? selectedCountry : countries[0]}
                onChange={(e) => setSelectedCountry(e.target.value)}
              >
                {countries.map((country) => (
                  <option key={country} value={country}>
                    {country}
                  </option>
                ))}
              </select>
              <label htmlFor="language">Language</label>
              <select id="language">
                {selectedLanguage ? (
                  languages.map((language) => (
                    <option
                      key={language}
                      value={language}
                      onChange={(e) => setSelectedLanguage(e.target.value)}
                    >
                      {language}
                    </option>
                  ))
                ) : (
                  <p>you should choose country first</p>
                )}
              </select>
              <label htmlFor="currency">Currency</label>
              <select id="currency">
                {selectedCurrency ? (
                  currencies.map((currency) => (
                    <option
                      key={currency}
                      value={currency}
                      onChange={(e) => setSelectedCurrency(e.target.value)}
                    >
                      {currency}
                    </option>
                  ))
                ) : (
                  <p>you should choose country first</p>
                )}
              </select>
            </div>
            <button className="modal-submit" onClick={handleOnclick}>
              CONTINUE
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}
