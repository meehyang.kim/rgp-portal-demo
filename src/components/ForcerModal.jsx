import React, { useEffect, useState } from "react";

export default function ForcerModal({
  title,
  items,
  isShowing,
  onSubmit,
  onClose,
}) {
  const [selectedItem, setSelectedItem] = useState(null);

  const handleOnClick = (e) => {
    e.stopPropagation();
    onSubmit(selectedItem);
    onClose();
  };

  const closeModal = (e) => {
    e.stopPropagation();
    onClose();
  };

  useEffect(() => {
    setSelectedItem(null);
  }, [isShowing]);

  if (!isShowing) return;

  return (
    <div className="modal-container">
      <div className="modal-col">
        <div className="modal">
          <div className="modal-content">
            <header className="modal-header">
              <h1 className="modal-title">{title}</h1>
              <button className="modal-close" onClick={closeModal}>X</button>
            </header>
            {!items || items.length === 0 ? (
              <p>no items</p>
            ) : (
              <ul>
                {items.map((item, index) => (
                  <li key={index} onClick={() => setSelectedItem(item)}>
                    {item.name}
                    {selectedItem === item ? " ✅" : ""}
                  </li>
                ))}
              </ul>
            )}
            <button className="modal-submit" disabled={!selectedItem} onClick={handleOnClick}>
              APPLY
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}
